# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aborted_Exam',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('exam', models.IntegerField()),
                ('owner', models.IntegerField()),
                ('class_associated', models.IntegerField()),
                ('aborted_time', models.DateTimeField(auto_now=True)),
                ('deletion_time', models.DateTimeField(default=None, null=True)),
                ('deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Expired_Exam',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('exam', models.IntegerField()),
                ('owner', models.IntegerField()),
                ('class_associated', models.IntegerField()),
                ('creation_time', models.DateTimeField(auto_now=True)),
                ('deletion_time', models.DateTimeField(default=None, null=True)),
                ('deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Ongoing_Exam',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('exam', models.IntegerField()),
                ('exam_end_time', models.DateTimeField()),
                ('owner', models.IntegerField()),
                ('class_associated', models.IntegerField()),
                ('is_paused', models.BooleanField(default=False)),
                ('creation_time', models.DateTimeField(auto_now=True)),
                ('deletion_time', models.DateTimeField(default=None, null=True)),
                ('deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Scheduled_Exam',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('exam', models.IntegerField()),
                ('exam_start_time', models.DateTimeField()),
                ('owner', models.IntegerField()),
                ('class_associated', models.IntegerField()),
                ('exam_end_time', models.DateTimeField()),
                ('creation_time', models.DateTimeField(auto_now=True)),
                ('deletion_time', models.DateTimeField(default=None, null=True)),
                ('deleted', models.BooleanField(default=False)),
            ],
        ),
    ]
