from time import time as _time,sleep
from threading import Timer
import threading
list =[]
def hello(val):
    list.insert(0, val)

def show():
    print(list.pop())
    
    
class _wait_me(threading.Thread):
    def __init__(self):
        self.chunks = 0.0005
        self.endtime =None
    """IMP NOTE    """ 
    """ wait function in every condition  usages time slice to check whether a resource requesting or requested for time """
    """ so i'm going to use these time chunks directly here with with our custom requirement and and these chunks has minimum and maximum time border also   """
    """ So there are 1ms to 1 second delay"""
    def wait_me(self,how_long_second=None):
        """ """
        if how_long_second is None:
            how_long_second=10000000000
            
        self.endtime=_time()+how_long_second
        while True:
            remaining = self.endtime-_time()
            if remaining <= 0:
                break
            self.chunks = min(self.chunks * 2, remaining, .05)
            sleep(self.chunks)

    def update_wait(self,new_time_second):
        """ update time chunk """
        self.endtime=_time()+new_time_second
        self.chunks = 0.0005
