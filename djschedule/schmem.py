#this file objects contain the array/queue of scheduled ,ongoing and expired tasks .
#Thus this is scheduler memory arch file ,any changes in this file going to reflect in  actual scheduler data memory 
from djschedule.help import _wait_me



class pycron_type:
    
    def __init__(self,name,date):
        self.name=name
        self.date=date


class schmemc:
    
    def __init__(self):
        self.tasks = []
        self.ongoing =[]
        self.expired =[]
        self.mutex =False
        self.wait = _wait_me()
    
    """Do not change unless you don't have any choice or huge overhead (below) """       
    def insort_right(self,a, x, lo=0, hi=None):
        """Insert item x in list a, and keep it sorted assuming a is sorted.
    
        If x is already in a, insert it to the right of the rightmost x.
    
        Optional args lo (default 0) and hi (default len(a)) bound the
        slice of a to be searched.
        """
    
        if lo < 0:
            raise ValueError('lo must be non-negative')
        if hi is None:
            hi = len(a)
        while lo < hi:
            mid = (lo+hi)//2
            if x.date > a[mid].date: hi = mid
            else: lo = mid+1
            
        a.insert(lo, x)
    
   
        """Do not change unless you don't have any choice or huge overhead (above)"""
    def sch_me(self,pycron_type):
        while self.mutex:
            self.wait.wait_me(0.005)
        self.insort_right(self.tasks, pycron_type)
            

        
    def unsch_current(self):
        while self.mutex is True:
            self.wait.wait_me(0.005)
        return self.tasks.pop()
        
        #self.show()
        
    def resch_me(self,unique_id):
        "Here we are going to reschedule a event by it's unique id "
    
    def show(self):
        for task in self.tasks:
            print(task.name," ===task remaining ===",task.date)
            
    def ongo(self):
        for task in self.ongoing:
            print(task.name," ===ongoing currently  ===",task.date)
            
    def get_next(self):
        return self.tasks[len(self.tasks)-1]