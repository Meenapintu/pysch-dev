from django.template.context_processors import request
from djschedule.models import Scheduled_Exam
from djschedule.schmem import pycron_type


def get_instance(id):
    data = Scheduled_Exam.objects.get(id=id)
    return data
