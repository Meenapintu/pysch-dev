from django.db import models
from datetime import datetime
from django.utils  import timezone
# Create your models here.
class Scheduled_Exam(models.Model):
    exam = models.IntegerField()
    exam_start_time = models.DateTimeField(auto_now=False, null= False)
    owner = models.IntegerField()
    class_associated = models.IntegerField()
    exam_end_time = models.DateTimeField(auto_now=False, null= False)
    creation_time = models.DateTimeField(auto_now=True)
    deletion_time = models.DateTimeField(auto_now=False, null = True, default=None)
    deleted = models.BooleanField(default=False)
    
    def move_to_ongoing(self):
        ongoing = Ongoing_Exam()
        ongoing.exam = self.exam
        ongoing.exam_end_time = self.exam_end_time
        ongoing.class_associated = self.class_associated
        ongoing.owner = self.owner
        obj = ongoing.save()
        self.deleted=True
        self.deletion_time=timezone.now()
        self.save()
        return obj
   
    def move_to_aborted(self):
        aborted = Aborted_Exam()
        aborted.exam = self.exam
        aborted.owner = self.owner
        aborted.class_associated = self.class_associated
        aborted.aborted_time = datetime.now()
        obj = aborted.save()
        self.deleted=True
        self.deletion_time=timezone.now()
        self.save()
        return obj


class Ongoing_Exam(models.Model):
    exam = models.IntegerField()
    exam_end_time = models.DateTimeField(auto_now=False, null= False)
    owner = models.IntegerField()
    class_associated = models.IntegerField()
    is_paused = models.BooleanField(default=False)
    creation_time = models.DateTimeField(auto_now=True)
    deletion_time = models.DateTimeField(auto_now=False, null = True, default=None)
    deleted = models.BooleanField(default=False)
    
    def move_to_expired(self):
        expired = Expired_Exam()
        expired.exam = self.exam
        expired.owner = self.owner
        expired.class_associated = self.class_associated
        obj = expired.save()
        self.deleted=True
        self.deletion_time=timezone.now()
        self.save()
        return obj
    
    def move_to_aborted(self):
        aborted = Aborted_Exam()
        aborted.exam = self.exam
        aborted.owner = self.owner
        aborted.class_associated = self.class_associated
        aborted.aborted_time = datetime.now()
        obj = aborted.save()
        self.deleted=True
        self.deletion_time=timezone.now()
        self.save()
        return obj


class Expired_Exam(models.Model):
    exam = models.IntegerField()
    owner = models.IntegerField()
    class_associated = models.IntegerField()
    creation_time = models.DateTimeField(auto_now=True)
    deletion_time = models.DateTimeField(auto_now=False, null = True, default=None)
    deleted = models.BooleanField(default=False)
    
    def move_to_scheduled(self, start_time, end_time):
        scheduled = Scheduled_Exam()
        scheduled.exam = self.exam
        scheduled.exam_start_time = start_time
        scheduled.exam_end_time = end_time
        scheduled.owner = self.owner
        scheduled.class_associated = self.class_associated
        obj = scheduled.save()
        self.deleted=True
        self.deletion_time=timezone.now()
        self.save()
        return obj


class Aborted_Exam(models.Model):
    exam = models.IntegerField()
    owner = models.IntegerField()
    class_associated = models.IntegerField()
    aborted_time = models.DateTimeField(auto_now=True)
    deletion_time = models.DateTimeField(auto_now=False, null = True, default=None)
    deleted = models.BooleanField(default=False)

    
    def move_to_scheduled(self, start_time, end_time):
        scheduled = Scheduled_Exam()
        scheduled.exam = self.exam
        scheduled.owner = self.owner
        scheduled.class_associated = self.class_associated
        scheduled.exam_start_time = start_time
        scheduled.exam_end_time = end_time
        obj = scheduled.save()
        self.deleted=True
        self.deletion_time=timezone.now()
        self.save()
        return obj
